import mlflow
import yaml
import importlib
from prefect import Flow, Parameter
from mle_platform.tasks.mle_tasks import MLETask


def get_exp(yaml_path, exp_name):
    with open(yaml_path) as f:
        data = yaml.load_all(f, Loader=yaml.FullLoader)
        for doc in data:
            if exp_name in doc:
                return doc
    return None


def get_handler(item):
    if 'handler' in item:
        _module = item['handler']['module']
        _class = item['handler']['class']
        _name = item['name']
        module = importlib.import_module(_module)
        class_ = getattr(module, _class)
        instance = MLETask(class_(contract=item.get('ro_contract')), name=_name)
        return instance
    return None


def parse_and_return(doc):
    if isinstance(doc, list):
        for indx, item in enumerate(doc):
            if 'name' in item:
                doc[indx] = get_handler(item)
            else:
                for key, value in item.items():
                    if isinstance(value, list):
                        for indx, item in enumerate(value):
                            value[indx] = get_handler(item)
    return doc


def _add(flow, prev_task, curr_task, **flow_args):
    flow.set_dependencies(
        task=curr_task, keyword_tasks={"ro": prev_task, **flow_args}
    )


def add_dependency(flow, data, **flow_args):
    ro = Parameter('ro', default=None)
    for indx, task in enumerate(data):
        if isinstance(task, MLETask):
            _add(flow, ro, task, **flow_args)
            ro = task
        else:
            _ro_list = []
            for key, value in task.items():
                _ro = ro
                for _indx, _task in enumerate(value):
                    _add(flow, _ro, _task, **flow_args)
                    _ro = _task
                _ro_list.append(_ro)
            ro = _ro_list


def get_flow(yaml_path, exp_name, exp_id):
    template_name = exp_name.split('_')[0]
    flow_config = get_exp(yaml_path, template_name)
    data = parse_and_return(flow_config.get(flow_config.get(template_name)))
    with Flow(exp_name) as flow:
        with mlflow.start_run() as run:
            mlflow.log_param('alpha', 1.9)
            flowargs = {'run_id': run.info.run_id, 'experiment_id': exp_id, 'nested': True}
            add_dependency(flow, data, **flowargs)
            # mlflow.end_run()
    return flow

