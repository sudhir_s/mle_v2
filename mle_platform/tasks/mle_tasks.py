import mlflow
from prefect import Task


class MLETask(Task):

    def __init__(self, handler, **kwargs):
        self.handler = handler
        super().__init__(**kwargs)

    def run(self, ro, **flow_kwargs):
        with mlflow.start_run(**flow_kwargs):
            self.handler.execute(ro)
            mlflow.log_metric("task name" + self.name, 123.90)
            return ro


