import abc


class Handler:
    def __init__(self, contract):
        self.contract = contract

    def pre_validate_ro(self, ro):
        print("----------pre validate", self.contract)
        if self.contract is not None:
            self.verify_keys(self.contract)

    def verify_keys(self, contract_dict):
        for k, v in contract_dict.items():
            if isinstance(v, dict):
                self.verify_keys(v)
            else:
                print(k, ":", v)

    def execute(self, ro):
        self.pre_validate_ro(ro)
        self.process(ro)
        return ro

    @abc.abstractmethod
    def process(self, ro):
        pass