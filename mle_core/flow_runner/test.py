import mlflow
import uuid, os
from mle_platform.flows.ml_flows import get_flow
from prefect.executors import DaskExecutor, LocalDaskExecutor

id = uuid.uuid1()
os.environ['MLFLOW_TRACKING_URI'] = 'http://0.0.0.0:9001'
os.environ['MLFLOW_S3_ENDPOINT_URL'] = 'http://192.168.13.136:9000'
os.environ['AWS_ACCESS_KEY_ID'] = 'minioadmin'
os.environ['AWS_SECRET_ACCESS_KEY'] = 'minioadmin'
exp_name = 'Anamoly Detection' + '_' + str(id)
exp_id = mlflow.create_experiment(exp_name, artifact_location='s3://mlobjectstore')
mlflow.set_experiment(exp_name)
flow = get_flow('/home/sudhirs/mle_tech_debt/mle_v2/mle_core/flow_config/flow.yaml', exp_name, exp_id)
# flow.visualize()
# flow.run()

# executor = LocalDaskExecutor()

executor = DaskExecutor(address="tcp://192.168.225.209:8786")
flow.run(executor=executor)
