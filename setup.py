from setuptools import setup, find_packages

setup(
    name='mle',
    version='0.1.0',
    packages=find_packages(include=['mle_core', 'mle_core.*', 'mle_platform', 'mle_platform.*'])
)
